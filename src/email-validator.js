const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'yandex.ru'];

export default function validate(email) {
  const emailParts = email.split('@');
  if (emailParts.length === 2) {
    const [, domain] = emailParts;
    return VALID_EMAIL_ENDINGS.includes(domain);
  }
  return false;
} 